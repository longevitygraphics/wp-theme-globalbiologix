<?php
$top_banner_bg    = get_field( 'top_banner_bg' );
$top_banner_text  = get_field( 'top_banner_text' );
$top_banner_style = '';
$top_banner_class = 'bg-aqua';
if ( $top_banner_bg and $top_banner_bg['url'] ) {
	$top_banner_style = 'background-image: url(' . $top_banner_bg['url'] . ')';
	$top_banner_class = 'top-banner--bg';
}
?>
<?php if ( $top_banner_text ) : ?>
    <div class="top-banner <?php echo $top_banner_class ?>" style="<?php echo $top_banner_style ?>">
        <div class="container">
            <div class="top-banner__content">
				<?php echo $top_banner_text ?>
            </div>
        </div>
    </div>
<?php endif; ?>