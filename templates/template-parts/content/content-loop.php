<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		
		<article>
			<header>
				<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="small"><i class="far fa-clock mr-1"></i><?php the_date(); ?><?php the_author_link(); ?></p>
			</header>
			<div class="article-content">
				<p><?php the_excerpt(); ?></p>
			</div>
			<footer class="small">
				Categories: <?php echo get_the_category_list(', '); ?> <br>
				<?php echo get_the_tag_list('Tags:&nbsp;', ', '); ?>
				<hr>
			</footer>
		</article>

	<?php endwhile; ?>
<?php endif ?>