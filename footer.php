<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<?php do_action( 'wp_content_bottom' ); ?>
</div>

<?php do_action( 'wp_body_end' ); ?>
<?php $lg_option_footer_site_legal = get_option( 'lg_option_footer_site_legal' ); ?>

<footer id="site-footer">

    <div>
        <div id="site-footer-main"
             class="clearfix pt-3 justify-content-center align-items-start flex-wrap">
            <div class="container">
                <div class="row">
                    <div class="site-footer-alpha col-sm-4 col-lg-2 text-center text-md-left"><?php dynamic_sidebar( 'footer-alpha' ); ?></div>
                    <div class="site-footer-bravo col-sm-5 col-lg-4 text-center text-md-left"><?php dynamic_sidebar( 'footer-bravo' ); ?></div>
                    <div class="site-footer-charlie col-sm-3 col-lg-6 text-center text-md-left"><?php dynamic_sidebar( 'footer-charlie' ); ?></div>
                </div>
            </div>
        </div>

		<?php if ( ! $lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable' ): ?>
            <div id="site-legal">
                <div class="container">
                    <div class="">
                        <div class="site-longevity text-center text-xl-right"> <?php get_template_part( "/templates/template-parts/footer/site-info" ); ?>
                            <a href="/privacy-policy/"> Privacy Policy </a>// <?php get_template_part( "/templates/template-parts/footer/site-footer-longevity" ); ?> </div>
                    </div>
                </div>
            </div>
		<?php endif; ?>
    </div>

</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action( 'document_end' ); ?>
